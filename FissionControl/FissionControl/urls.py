"""FissionControl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from fans import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/servers/', views.ServerList.as_view()),
    path('api/servers/<uuid:UUID>', views.ServerDetail.as_view()),
    path('api/zones/', views.ZoneList.as_view()),
    path('api/zones/<uuid:UUID>', views.ZoneDetail.as_view()),
    path('api/zonetemps/', views.ZoneTempList.as_view()),
    path('api/zonetemps/<uuid:UUID>', views.ZoneTempDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)