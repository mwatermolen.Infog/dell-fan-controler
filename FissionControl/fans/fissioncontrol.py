from typing import Dict
from django.utils import timezone
import uuid, logging
from fans.models import Server, Zone, ZoneTemp
from fans.impi import IMPI


def optimize_fans(SystemUUID):
    systemX:Server = Server.objects.get(UUID=SystemUUID)

    logging.info(f"[+] Starting Server {systemX.name}")

    HOT = True
    HOT_HYS= True
    COLD = True
    COLD_HYS= True

    iDRAC = IMPI(systemX.idrac_ip, systemX.idrac_user, systemX.idrac_pass, systemX.idrac_ipmi_key)

    if not systemX.enable_temp_ctl:
        iDRAC.set_fans_auto()
        logging.info(f"[+] Server {systemX.name} - STATUS: AUTOMATIC")
        return True

    Temps =  iDRAC.get_zone_temps()

    ZonesStatus = []

    if len(systemX.tempZone.all()) == 0:
        for temp in Temps:
            if temp[1] >= systemX.max_zone_temp:
                data = {"status": "hot"}
            elif temp[1] >= systemX.max_zone_temp - 5:
                data = {"status": "hot_hys"}
            elif temp[1] <= systemX.min_zone_temp - 5:
                data = {"status": "cold_hys"}
            elif temp[1] <= systemX.min_zone_temp :
                data = {"status": "cold"}
            else:
                data = {"status": "normal"}
            ZonesStatus.append(data)
            ZTX = ZoneTemp(
                UUID=uuid.uuid4(),
                time=timezone.now(),
                temp=temp[1]
            )
            ZTX.save()
            ZoneX= Zone(
                name=temp[0],
                ServerUUID=systemX.UUID,
                lastUpdated = timezone.now(), 
            )
            ZoneX.save()
            ZoneX.temps.add(ZTX)
            ZoneX.save()
            systemX.tempZone.add(ZoneX)
            systemX.save()
                
    else:
        for ZoneX in systemX.tempZone.all():
            ZoneX:Zone
            for temp in Temps:
                if ZoneX.name == temp[0] and ZoneX.ServerUUID == systemX.UUID:
                    if temp[1] >= systemX.max_zone_temp:
                        data = {"status": "hot"}
                    elif temp[1] >= systemX.max_zone_temp - 5:
                        data = {"status": "hot_hys"}
                    elif temp[1] <= systemX.min_zone_temp - 5:
                        data = {"status": "cold_hys"}
                    elif temp[1] <= systemX.min_zone_temp :
                        data = {"status": "cold"}
                    else:
                        data = {"status": "normal"}
                    ZonesStatus.append(data)
                    ZTX = ZoneTemp(
                        UUID=uuid.uuid4(),
                        time=timezone.now(),
                        temp=temp[1]
                    )
                    ZTX.save()

                    ZoneX.temps.add(ZTX)
                    ZoneX.lastUpdated = timezone.now()
                    ZoneX.save()

    Condition = "normal"
    Conditions = []
    for statusX in ZonesStatus:
        if statusX["status"] != "normal":
            Condition =  statusX["status"]
            Conditions.append(statusX["status"])

    Conditions = list(dict.fromkeys(Conditions)) 

    if "normal" in Conditions:
        del Conditions["normal"]

    if len(Conditions) > 1:
        logging.error(f"[!] CONFLITING TEMP DATA - {Conditions}")
        if "hot" in Conditions:
            Condition=  "hot"
        elif "hot_hys" in Conditions:
            Condition=  "hot"
        elif "cold" in Conditions:
            Condition=  "cold"



    if Condition == "cold":
        iDRAC.set_fans_man(iDRAC.commands.speeds.min)
    elif Condition == "cold_hys":
        iDRAC.set_fans_man(iDRAC.commands.speeds.S02)
    elif Condition == "hot_hys":
        iDRAC.set_fans_man(iDRAC.commands.speeds.S14)
    elif Condition == "hot":
        iDRAC.set_fans_auto()
        
    if Condition == "normal":
        if systemX.normal_operation_mode == "auto":
            iDRAC.set_fans_auto()            
        else:
            iDRAC.set_fans_man([systemX.normal_operation_mode])
    logging.info(f"[+] Server {systemX.name} - STATUS: {Condition}")
