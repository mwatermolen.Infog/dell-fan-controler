from celery import shared_task
from fans.models import Server
from fans.fissioncontrol import optimize_fans

@shared_task
def check_temps():
    for SeverX in Server.objects.all():
        optimize_fans_exec.delay(SeverX.UUID)

@shared_task
def optimize_fans_exec(SeverXUUID):
    optimize_fans(SeverXUUID)