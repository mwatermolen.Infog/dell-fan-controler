FROM python:3.9

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set work directory
WORKDIR /code

RUN apt update && apt install -y ipmitool python3-mysqldb libmariadb-dev python-dev

# install dependencies
RUN pip install --upgrade pip 
COPY requirements.txt /code/
RUN pip install -r requirements.txt

COPY ./FissionControl/ /code/
ADD uwsgi.conf /code/

#RUN mkdir /code/static
VOLUME /code/static



EXPOSE 8000

#CMD ["python", "/code/manage.py", "runserver", "0.0.0.0:8000"]