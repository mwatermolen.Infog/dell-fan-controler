from rest_framework import serializers
from fans.models import Server, ZoneTemp, Zone

class ServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Server
        fields = ['UUID', 'name', 'idrac_ip', 'idrac_user', 'idrac_pass', 'idrac_ipmi_key', 'enable_temp_ctl', 'tempZone', 'max_zone_temp', 'min_zone_temp', 'normal_operation_mode', 'health_check_url']

class ZoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zone
        fields = ['UUID', 'name', 'ServerUUID', 'lastUpdated', 'temps']

class ZoneTempSerializer(serializers.ModelSerializer):
    class Meta:
        model = ZoneTemp
        fields = ['UUID', 'time', 'temp']