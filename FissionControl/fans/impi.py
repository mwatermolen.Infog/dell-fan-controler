import datetime
import uuid
import json
import subprocess
import logging

class IMPI():
    def __init__(self, IP, USER, PASS, KEY):
        self.ip = IP
        self.user = USER
        self.password = PASS
        self.key = KEY

            
    class commands:
        Auto = ['raw','0x30', '0x30', '0x01', '0x01']   
        enable =  ['raw','0x30', '0x30', '0x01', '0x00']
        speedset = ['raw','0x30', '0x30', '0x02', '0xff']
        class speeds:
                default = ['0x12']
                min = ['0x02']
                max = ['0x28']
                S01 = ['0x02']
                S02 = ['0x04']
                S03 = ['0x06']
                S04 = ['0x08']
                S05 = ['0x0A']
                S06 = ['0x0C']
                S07 = ['0x0E']
                S08 = ['0x10']
                S09 = ['0x12']
                S10 = ['0x14']
                S11 = ['0x16']
                S12 = ['0x18']
                S13 = ['0x1A']
                S14 = ['0x1C']
                S15 = ['0x1E']
                S16 = ['0x20']
                S17 = ['0x22']
                S18 = ['0x24']
                S19 = ['0x26']
                S20 = ['0x28']


    def run_impi_command(self, CMD):
        if isinstance(CMD, str): CMD = CMD.split(' ')
        baseCommand = ["ipmitool", "-I", "lanplus", "-H" , self.ip, "-U", self.user, "-P", self.password, "-y", self.key]
        IPMICommand = baseCommand + CMD
        logging.debug(f"[+] Running IPMI CMD {IPMICommand}")
        result = subprocess.run(IPMICommand, stdout=subprocess.PIPE).stdout.decode('utf-8')
        return result

    def get_zone_temps(self):
        IPMIDATA =[]
        zones = []
        data = self.run_impi_command('sdr type temperature')

        for line in data.splitlines():											#SPLIT IPMI DATA
                dataobj=[]
                for data in line.split('|'):
                        dataobj.append(data.strip())
                IPMIDATA.append(dataobj)


        for id, data in enumerate(IPMIDATA):													#Rename Duplacte zones
            try:    
                zones.append([id, data[0], int(data[4].split(' ')[0])])
            except: pass

        i = 1
        for ZoneNameA in zones:
            for ZoneNameB in zones:
                if ZoneNameA[1] == ZoneNameB[1] and ZoneNameA[0] != ZoneNameB[0]:
                    zones[ZoneNameA[0]][1] = ZoneNameA[1] + "_" + str(i)
                    i =+ i
        
        for zone in zones:
            del zone[0]


        return zones

    def set_fans_auto(self):
        logging.info(f"[+] Setting Fans to Auto")
        self.run_impi_command(self.commands.Auto)

    def set_fans_man(self, speed):
        logging.warning(f"[+] Setting Fans to Man: {speed}")
        self.run_impi_command(self.commands.enable)
        self.run_impi_command(self.commands.speedset + speed)