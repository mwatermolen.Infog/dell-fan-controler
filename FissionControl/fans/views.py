from fans.models import Server, Zone, ZoneTemp
from fans.serializers import ZoneSerializer, ZoneTempSerializer, ServerSerializer
from django.http import Http404
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

class ServerList(APIView):
    """
    List all Servers, or create a new Server.
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self, request, format=None):
        Servers = Server.objects.all()
        serializer = ServerSerializer(Servers, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ServerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ServerDetail(APIView):
    """
    Retrieve, update or delete a Server instance.
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    def get_object(self, UUID):
        try:
            return Server.objects.get(UUID=UUID)
        except Server.DoesNotExist:
            raise Http404

    def get(self, request, UUID, format=None):
        Server = self.get_object(UUID)
        serializer = ServerSerializer(Server)
        return Response(serializer.data)

    def put(self, request, UUID, format=None):
        Server = self.get_object(UUID)
        serializer = ServerSerializer(Server, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, UUID, format=None):
        Server = self.get_object(UUID)
        Server.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ZoneList(APIView):
    """
    List all Zones, or create a new Zone.
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self, request, format=None):
        Zones = Zone.objects.all()
        serializer = ZoneSerializer(Zones, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ZoneSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ZoneDetail(APIView):
    """
    Retrieve, update or delete a Zone instance.
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    def get_object(self, UUID):
        try:
            return Zone.objects.get(UUID=UUID)
        except Zone.DoesNotExist:
            raise Http404

    def get(self, request, UUID, format=None):
        Zone = self.get_object(UUID)
        serializer = ZoneSerializer(Zone)
        return Response(serializer.data)

    def put(self, request, UUID, format=None):
        Zone = self.get_object(UUID)
        serializer = ZoneSerializer(Zone, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, UUID, format=None):
        Zone = self.get_object(UUID)
        Zone.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ZoneTempList(APIView):
    """
    List all ZoneTemps, or create a new ZoneTemp.
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self, request, format=None):
        ZoneTemps = ZoneTemp.objects.all()
        serializer = ZoneTempSerializer(ZoneTemps, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ZoneTempSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ZoneTempDetail(APIView):
    """
    Retrieve, update or delete a ZoneTemp instance.
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    def get_object(self, UUID):
        try:
            return ZoneTemp.objects.get(UUID=UUID)
        except ZoneTemp.DoesNotExist:
            raise Http404

    def get(self, request, UUID, format=None):
        ZoneTemp = self.get_object(UUID)
        serializer = ZoneTempSerializer(ZoneTemp)
        return Response(serializer.data)

    def put(self, request, UUID, format=None):
        ZoneTemp = self.get_object(UUID)
        serializer = ZoneTempSerializer(ZoneTemp, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, UUID, format=None):
        ZoneTemp = self.get_object(UUID)
        ZoneTemp.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
