from datetime import datetime
import uuid
from django.db import models

# Create your models here.

class ZoneTemp(models.Model):
    UUID = models.UUIDField(default=uuid.uuid4, primary_key=True)
    time = models.DateTimeField(default=datetime.now)
    temp = models.IntegerField()

    def __str__(self) -> str:
        return self.time.isoformat() + " " + str(self.temp)+  "C"

class Zone (models.Model):
    UUID = models.UUIDField(default=uuid.uuid4, primary_key=True)
    name = models.CharField(max_length=255)
    ServerUUID = models.UUIDField(null=True, blank=True)
    lastUpdated = models.DateTimeField(default=datetime.now)
    temps = models.ManyToManyField(ZoneTemp)

    def __str__(self) -> str:
        return self.name

class Server (models.Model):

    OP_MODE_CHCICES = (
        ("auto","Auto"),
        ("0x02", "Min"),
        ("0x28", "Max"),
        ("0x04", "~1700 RPM"),
        ("0x06", "~1800 RPM"),
        ("0x08", "~2000 RPM"),
        ("0x0A", "~2200 RPM"),
        ("0x0C", "~2500 RPM"),
        ("0x0E", "~2700 RPM"),
        ("0x10", "~3000 RPM"),
        ("0x12", "~3200 RPM"),
        ("0x14", "~3400 RPM"),
        ("0x16", "~3600 RPM"),
        ("0x18", "~3800 RPM"),
        ("0x1A", "~4000 RPM"),
        ("0x1C", "~4200 RPM"),
        ("0x1E", "~4600 RPM"),
        ("0x20", "~4800 RPM"),
        ("0x22", "~5000 RPM"),
        ("0x24", "~5200 RPM"),
        ("0x26", "~5400 RPM")
    )

    UUID = models.UUIDField(default=uuid.uuid4, primary_key=True)
    name = models.CharField(max_length=255)
    idrac_ip = models.CharField('iDRAC IP',max_length=255)
    idrac_user = models.CharField('iDRAC USER',max_length=255)
    idrac_pass = models.CharField('iDRAC PASS',max_length=255)
    idrac_ipmi_key = models.CharField('iDRAC IMPI Key',max_length=255)
    enable_temp_ctl = models.BooleanField('Enable Temp Control')
    tempZone = models.ManyToManyField(Zone, blank=True)
    max_zone_temp = models.IntegerField()
    min_zone_temp = models.IntegerField()
    normal_operation_mode = models.CharField(max_length=255, choices=OP_MODE_CHCICES)
    health_check_url = models.CharField(max_length=255, blank=True)

    def __str__(self) -> str:
        return self.name
