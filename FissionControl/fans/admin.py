from django.contrib import admin

from fans.models import Server, ZoneTemp, Zone
# Register your models here.
admin.site.register(Server)
admin.site.register(ZoneTemp)
admin.site.register(Zone)